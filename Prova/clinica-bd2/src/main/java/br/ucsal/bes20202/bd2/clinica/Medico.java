package br.ucsal.bes20202.bd2.clinica;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "medico", uniqueConstraints = @UniqueConstraint(columnNames = { "numeroCRM", "ufCRM" }))
public class Medico extends Funcionario {

	@Column(name = "numeroCRM", columnDefinition = " char(11) ", nullable = false)
	private String numeroCRM;

	@ManyToOne
	@JoinColumn(name = "ufCRM", nullable = false)
	private UF ufCRM;

	@ManyToMany
	private List<Especialidade> especialidades;

	public Medico() {
	}

	public Medico(Integer numeroRg, String orgaoExpedidorRg, UF ufRg, LocalDate dataAdmissao, String nome,
			List<String> telefones, String numeroCRM, UF ufCRM, List<Especialidade> especialidades) {
		super(numeroRg, orgaoExpedidorRg, ufRg, dataAdmissao, nome, telefones);
		this.numeroCRM = numeroCRM;
		this.ufCRM = ufCRM;
		this.especialidades = especialidades;
	}

	public String getNumeroCRM() {
		return numeroCRM;
	}

	public void setNumeroCRM(String numeroCRM) {
		this.numeroCRM = numeroCRM;
	}

	public UF getUfCRM() {
		return ufCRM;
	}

	public void setUfCRM(UF ufCRM) {
		this.ufCRM = ufCRM;
	}

	public List<Especialidade> getEspecialidades() {
		return especialidades;
	}

	public void setEspecialidades(List<Especialidade> especialidades) {
		this.especialidades = especialidades;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((especialidades == null) ? 0 : especialidades.hashCode());
		result = prime * result + ((numeroCRM == null) ? 0 : numeroCRM.hashCode());
		result = prime * result + ((ufCRM == null) ? 0 : ufCRM.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Medico other = (Medico) obj;
		if (especialidades == null) {
			if (other.especialidades != null)
				return false;
		} else if (!especialidades.equals(other.especialidades))
			return false;
		if (numeroCRM == null) {
			if (other.numeroCRM != null)
				return false;
		} else if (!numeroCRM.equals(other.numeroCRM))
			return false;
		if (ufCRM == null) {
			if (other.ufCRM != null)
				return false;
		} else if (!ufCRM.equals(other.ufCRM))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Medico [numeroCRM=" + numeroCRM + ", ufCRM=" + ufCRM + ", especialidades=" + especialidades
				+ ", toString()=" + super.toString() + "]";
	}

}
