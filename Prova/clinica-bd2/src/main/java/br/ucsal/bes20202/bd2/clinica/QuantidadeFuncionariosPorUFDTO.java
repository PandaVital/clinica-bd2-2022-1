package br.ucsal.bes20202.bd2.clinica;

public class QuantidadeFuncionariosPorUFDTO {
	private UF uf;
	private Long qtdFuncionario;

	public QuantidadeFuncionariosPorUFDTO(UF uf, Long qtdFuncionario) {
		super();
		this.uf = uf;
		this.qtdFuncionario = qtdFuncionario;
	}

	@Override
	public String toString() {
		return "QuantidadeFuncionariosPorUFDTO [uf=" + uf + ", qtdFuncionario=" + qtdFuncionario + "]";
	}

	public QuantidadeFuncionariosPorUFDTO() {

	}

	public UF getUf() {
		return uf;
	}

	public void setUf(UF uf) {
		this.uf = uf;
	}

	public Long getQtdFuncionario() {
		return qtdFuncionario;
	}

	public void setQtdFuncionario(Long qtdFuncionario) {
		this.qtdFuncionario = qtdFuncionario;
	}

}
