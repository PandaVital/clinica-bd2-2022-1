package br.ucsal.bes20202.bd2.clinica;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main {

	private static EntityManager em;
	private static LocalDate hoje = LocalDate.now();

	public static void main(String[] args) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("clinicabd2");
		em = emf.createEntityManager();
		populacao();

		ConsultasJPA.quantidadeFuncionariosPorUF(em);

	}

	public static void populacao() {
		List<String> telefone01 = new ArrayList<String>();
		List<String> telefonemedico01 = new ArrayList<String>();
		List<String> telefonemedico02 = new ArrayList<String>();
		List<Especialidade> especmed1 = new ArrayList<Especialidade>();
		List<Especialidade> especmed2 = new ArrayList<Especialidade>();

		telefone01.add("(71)99290-7648");
		telefonemedico01.add("(31)11111-2222");
		telefonemedico02.add("(71)22222-3333");

		em.getTransaction().begin();
		UF ufBA = new UF("BA", "BAHIA");
		UF ufSP = new UF("SP", "S�O PAULO");

		Funcionario f1 = new Funcionario(1234567890, "SSP", ufBA, hoje, "Jo�o", telefone01);

		// MEDICO 1
		Especialidade especialidade1 = new Especialidade("Neurologista", null);
		Especialidade especialidade2 = new Especialidade("Clinico", null);
		especmed1.add(especialidade1);
		especmed1.add(especialidade2);
		Medico med1 = new Medico(2222, "SSP", ufSP, hoje, "Pedro", telefonemedico01, "02222", ufSP, especmed1);

		// MEDICO 2
		Especialidade especialidade3 = new Especialidade("Dermatologista", null);
		Especialidade especialidade4 = new Especialidade("Urologista", null);
		especmed2.add(especialidade3);
		especmed2.add(especialidade4);
		Medico med2 = new Medico(3333, "SSP", ufBA, hoje, "Paulo", telefonemedico02, "03333", ufBA, especmed2);

		Especialidade especialidade5 = new Especialidade("Dentista", null);

		persistAll(ufBA, ufSP, especialidade1, especialidade2, especialidade3, especialidade4, especialidade5, f1, med1,
				med2);
		em.getTransaction().commit();

	}

	public static void persistAll(Object... objects) {

		for (Object o : objects) {
			em.persist(o);
		}

	}
}
