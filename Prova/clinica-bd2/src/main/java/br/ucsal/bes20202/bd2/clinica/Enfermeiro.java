package br.ucsal.bes20202.bd2.clinica;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "enfermeiro", uniqueConstraints = @UniqueConstraint(columnNames = { "coren" }))
public class Enfermeiro extends Funcionario {

	@Column(name = "Coren", columnDefinition = " char(10) ", nullable = false)
	private String numeroRegistroCoren;

	public Enfermeiro() {
	}

	public Enfermeiro(Integer numeroRg, String orgaoExpedidorRg, UF ufRg, LocalDate dataAdmissao, String nome,
			List<String> telefones, String numeroCRM, UF ufCRM, List<Especialidade> especialidades) {
		super(numeroRg, orgaoExpedidorRg, ufRg, dataAdmissao, nome, telefones);
		this.numeroRegistroCoren = numeroCRM;

	}

	public String getNumeroRegistroCoren() {
		return numeroRegistroCoren;
	}

	public void setNumeroRegistroCoren(String numeroRegistroCoren) {
		this.numeroRegistroCoren = numeroRegistroCoren;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(numeroRegistroCoren);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Enfermeiro other = (Enfermeiro) obj;
		return Objects.equals(numeroRegistroCoren, other.numeroRegistroCoren);
	}

	@Override
	public String toString() {
		return "Enfermeiro [numeroRegistroCoren=" + numeroRegistroCoren + "]";
	}

	public Enfermeiro(String numeroRegistroCoren) {
		super();
		this.numeroRegistroCoren = numeroRegistroCoren;
	}

	public Enfermeiro(Integer numeroRg, String orgaoExpedidorRg, UF ufRg, LocalDate dataAdmissao, String email,
			List<String> telefones) {
		super(numeroRg, orgaoExpedidorRg, ufRg, dataAdmissao, email, telefones);
	}

}
