package br.ucsal.bes20202.bd2.clinica;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

public class ConsultasJPA {



	public static void quantidadeFuncionariosPorUF(EntityManager em) {
		TypedQuery<QuantidadeFuncionariosPorUFDTO> tq = em.createQuery(
				"select new br.ucsal.bes20202.bd2.clinica.QuantidadeFuncionariosPorUFDTO(f.ufRg, count(*)) from Funcionario f group by f.ufRg",
				QuantidadeFuncionariosPorUFDTO.class);
		tq.getResultList();
	}

}
