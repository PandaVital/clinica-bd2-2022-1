package br.ucsal.bes20202.bd2.clinica;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@SequenceGenerator(initialValue = 1, sequenceName = "matricula_id", name = "matricula")
@Table(uniqueConstraints = {
		@UniqueConstraint(name = "unique_funcionario", columnNames = { "numeroRg", "orgaoExpedidorRg", "ufRg" }) })
public class Funcionario {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "matricula")
	private Integer matricula;

	@Column(nullable = false)
	private Integer numeroRg;

	@Column(columnDefinition = "varchar(40)", nullable = false)
	private String orgaoExpedidorRg;

	@ManyToOne
	@JoinColumn(name = "ufRg")
	private UF ufRg;

	@Column(nullable = false)
	private LocalDate dataAdmissao;

	@Column(columnDefinition = "varchar(40)")
	private String nome;

	@ElementCollection
	private List<String> telefones;

	public Funcionario() {
	}

	public Funcionario(Integer numeroRg, String orgaoExpedidorRg, UF ufRg, LocalDate dataAdmissao, String email,
			List<String> telefones) {
		this.numeroRg = numeroRg;
		this.orgaoExpedidorRg = orgaoExpedidorRg;
		this.ufRg = ufRg;
		this.dataAdmissao = dataAdmissao;
		this.nome = email;
		this.telefones = telefones;
	}

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	public Integer getNumeroRg() {
		return numeroRg;
	}

	public void setNumeroRg(Integer numeroRg) {
		this.numeroRg = numeroRg;
	}

	public String getOrgaoExpedidorRg() {
		return orgaoExpedidorRg;
	}

	public void setOrgaoExpedidorRg(String orgaoExpedidorRg) {
		this.orgaoExpedidorRg = orgaoExpedidorRg;
	}

	public UF getUfRg() {
		return ufRg;
	}

	public void setUfRg(UF ufRg) {
		this.ufRg = ufRg;
	}

	public LocalDate getDataAdmissao() {
		return dataAdmissao;
	}

	public void setDataAdmissao(LocalDate dataAdmissao) {
		this.dataAdmissao = dataAdmissao;
	}

	public String getEmail() {
		return nome;
	}

	public void setEmail(String email) {
		this.nome = email;
	}

	public List<String> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<String> telefones) {
		this.telefones = telefones;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataAdmissao == null) ? 0 : dataAdmissao.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((matricula == null) ? 0 : matricula.hashCode());
		result = prime * result + ((numeroRg == null) ? 0 : numeroRg.hashCode());
		result = prime * result + ((orgaoExpedidorRg == null) ? 0 : orgaoExpedidorRg.hashCode());
		result = prime * result + ((telefones == null) ? 0 : telefones.hashCode());
		result = prime * result + ((ufRg == null) ? 0 : ufRg.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Funcionario other = (Funcionario) obj;
		if (dataAdmissao == null) {
			if (other.dataAdmissao != null)
				return false;
		} else if (!dataAdmissao.equals(other.dataAdmissao))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (matricula == null) {
			if (other.matricula != null)
				return false;
		} else if (!matricula.equals(other.matricula))
			return false;
		if (numeroRg == null) {
			if (other.numeroRg != null)
				return false;
		} else if (!numeroRg.equals(other.numeroRg))
			return false;
		if (orgaoExpedidorRg == null) {
			if (other.orgaoExpedidorRg != null)
				return false;
		} else if (!orgaoExpedidorRg.equals(other.orgaoExpedidorRg))
			return false;
		if (telefones == null) {
			if (other.telefones != null)
				return false;
		} else if (!telefones.equals(other.telefones))
			return false;
		if (ufRg == null) {
			if (other.ufRg != null)
				return false;
		} else if (!ufRg.equals(other.ufRg))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Funcionario [matricula=" + matricula + ", numeroRg=" + numeroRg + ", orgaoExpedidorRg="
				+ orgaoExpedidorRg + ", ufRg=" + ufRg + ", dataAdmissao=" + dataAdmissao + ", email=" + nome
				+ ", telefones=" + telefones + "]";
	}

}
