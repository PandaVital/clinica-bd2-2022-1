package br.ucsal.bes20221.bd2.clinica;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Medico {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer matricula;

	private LocalDate dataAdmissao;

	@Column(columnDefinition = "varchar(255)")
	private String email;

	@ElementCollection
	@Column(columnDefinition = "varchar(15)")
	private List<String> telefones;

	@Column(columnDefinition = "char(11)")
	private String numeroCRM;

	private UF ufCRM;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "especialidades")
	private List<Especialidade> especialidades;

	public Medico() {
	}

	public Medico(Integer matricula, String numeroCRM, UF ufCRM, LocalDate dataAdmissao, String email,
			List<String> telefones, List<Especialidade> especialidades) {
		super();
		this.matricula = matricula;
		this.numeroCRM = numeroCRM;
		this.ufCRM = ufCRM;
		this.dataAdmissao = dataAdmissao;
		this.email = email;
		this.telefones = telefones;
		this.especialidades = especialidades;
	}

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	public String getNumeroCRM() {
		return numeroCRM;
	}

	public void setNumeroCRM(String numeroCRM) {
		this.numeroCRM = numeroCRM;
	}

	public UF getUfCRM() {
		return ufCRM;
	}

	public void setUfCRM(UF ufCRM) {
		this.ufCRM = ufCRM;
	}

	public LocalDate getDataAdmissao() {
		return dataAdmissao;
	}

	public void setDataAdmissao(LocalDate dataAdmissao) {
		this.dataAdmissao = dataAdmissao;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<String> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<String> telefones) {
		this.telefones = telefones;
	}

	public List<Especialidade> getEspecialidades() {
		return especialidades;
	}

	public void setEspecialidades(List<Especialidade> especialidades) {
		this.especialidades = especialidades;
	}

	@Override
	public int hashCode() {
		return Objects.hash(dataAdmissao, email, especialidades, matricula, numeroCRM, telefones, ufCRM);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Medico other = (Medico) obj;
		return Objects.equals(dataAdmissao, other.dataAdmissao) && Objects.equals(email, other.email)
				&& Objects.equals(especialidades, other.especialidades) && Objects.equals(matricula, other.matricula)
				&& Objects.equals(numeroCRM, other.numeroCRM) && Objects.equals(telefones, other.telefones)
				&& Objects.equals(ufCRM, other.ufCRM);
	}

	@Override
	public String toString() {
		return "Medico [matricula=" + matricula + ", dataAdmissao=" + dataAdmissao + ", email=" + email + ", telefones="
				+ telefones + ", numeroCRM=" + numeroCRM + ", ufCRM=" + ufCRM + ", especialidades=" + especialidades
				+ "]";
	}

}
