package br.ucsal.bes20221.bd2.clinica;

import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Especialidade {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer codigo;

	@Column(columnDefinition = "varchar(50)")
	private String nome;
	
	@OneToMany
	@JoinColumn(name = "Medico")
	private List<Medico> medicos;

	public Especialidade() {
	}

	public Especialidade(Integer codigo, String nome, List<Medico> medicos) {
		this.codigo = codigo;
		this.nome = nome;
		this.medicos = medicos;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Medico> getMedicos() {
		return medicos;
	}

	public void setMedicos(List<Medico> medicos) {
		this.medicos = medicos;
	}

	@Override
	public int hashCode() {
		return Objects.hash(codigo, medicos, nome);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Especialidade other = (Especialidade) obj;
		return Objects.equals(codigo, other.codigo) && Objects.equals(medicos, other.medicos)
				&& Objects.equals(nome, other.nome);
	}

	@Override
	public String toString() {
		return "Especialidade [codigo=" + codigo + ", nome=" + nome + ", medicos=" + medicos + "]";
	}

}
