package br.ucsal.bes20221.bd2.clinica;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public class Main {

	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("aula01jpa");
		EntityManager em = emf.createEntityManager();

		

		em.getTransaction().begin();
		
		em.getTransaction().commit();

		
		emf.close();
	}

}
